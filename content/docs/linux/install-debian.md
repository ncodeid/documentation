---
title: "Install Debian"
date: "Mar 08, 2024"
draft: false
---

Laporan Installasi Debian 11 

[//]: # ({{< image src="../../images/logo.png" alt="This is sample image" >}})

![Image](../../images/logo.png)

## Apa itu Debian?

Debian adalah sistem operasi yang gratis dan open source, menawarkan stabilitas, keamanan, dan fleksibilitas untuk
berbagai kebutuhan. Baik kamu pengguna komputer biasa, developer, ataupun perusahaan.

## Dipakai Untuk Apa Sih!

Debian digunakan hampir di seluruh dunia dengan performa dan kecepatan yang handal dengan menggunakan sumber daya seminimal mungkin

### Fitur Utama Debian:

1. Gratis dan Open Source: Bebas digunakan dan dimodifikasi tanpa biaya lisensi.
2. Stabilitas Tinggi: Terkenal dengan stabilitasnya yang teruji, ideal untuk server dan desktop.
3. Keamanan Terjamin: Sistem keamanan yang kuat dan terpercaya.
4. Pilihan Perangkat Lunak Lengkap: Ribuan aplikasi tersedia untuk berbagai kebutuhan.
5. Kompatibilitas Hardware Luas: Mendukung berbagai jenis hardware komputer.
6. Komunitas Aktif: Dukungan komunitas yang besar dan ramah.

## Langkah Installasi Debian:

- ### Melakukan Install Iso:
1. Buka [https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/) lalu scroll kebawah dan cari netinst.iso<br/>![install](../../../images/install.png)
2. Lalu pilih aplikasi virtual machine yang ingin di gunakan<br/>![vm-image](../../../images/vm_list.jpg)
- ### Install Melalui Virtual Machine:
1. Create a new Virtual Machine
2. lalu pilih Typical (recommended)
3. Masukkan iso yang sudah di download tadi
4. Pada Menu Select a Guest Operating System pilih Linux lalu Debian xx.xx
5. Lalu Beri Nama Virtual Machine
6. Masukkan ukuran kapasitas disk yang ingin di gunakan, Untuk Store disk as a single file ataupun multiple files Sama Saja
7. Untuk menyelesaikan settingan dapat pilih Finish pada menu Ready to Create Virtual Machine

### Tahap Installasi dalam VM:
1. Boot VM dari ISO Debian:
   + Pilih file ISO Debian yang telah diunduh pada langkah persiapan.
   + Nyalakan mesin virtual.
2. Pilih bahasa: Pilih bahasa yang ingin digunakan selama proses instalasi.
3. Pilih jenis instalasi: Pilih "Install" untuk melakukan instalasi standar.
4. Konfigurasi jaringan: Pilih jenis konfigurasi jaringan yang sesuai dengan kebutuhan Anda. 
5. Partisi disk: Pilih skema partisi yang diinginkan.
   + Opsi "Use entire disk" direkomendasikan untuk pengguna baru.
6. Buat akun pengguna:
   + Masukkan nama dan password untuk akun pengguna utama.
   + Anda juga dapat memilih untuk membuat akun administrator tambahan.
7. Instalasi perangkat lunak: Pilih paket perangkat lunak yang ingin diinstal selama proses instalasi.
   + Anda dapat memilih instalasi minimal, desktop, atau server.
8. Bootloader: Pilih bootloader yang ingin digunakan.
   + GRUB bootloader direkomendasikan untuk kebanyakan pengguna.
9. Konfigurasi tambahan:
   + Konfigurasi zona waktu, hostname, dan pengaturan lainnya.
10. Selesai: Proses instalasi Debian akan selesai setelah beberapa menit.

### Setelah Boot ke Debian (Pasca Install):
1. Perbarui System menggunakan command atau perintah
   {{< code-highlight bash >}}
   sudo apt-get update && sudo apt-get upgrade
   {{< /code-highlight >}}







